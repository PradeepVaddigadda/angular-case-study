import { NgModule } from '@angular/core';
import{MatDatepickerModule} from '@angular/material/datepicker';
import{ MatNativeDateModule} from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule} from '@angular/material/form-field'; 
import {MatTableModule} from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
const MaterialComponents=[
  MatButtonModule,
  MatDatepickerModule,
  MatNativeDateModule,
  BrowserAnimationsModule,
  MatFormFieldModule,
  MatTableModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatCardModule,
  MatIconModule,
  
]

@NgModule({
  

  imports: [MaterialComponents],
  exports:[MaterialComponents]
})
export class MaterialModule { }
