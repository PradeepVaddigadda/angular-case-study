import { Component, OnInit } from '@angular/core';
import { StudentAttendanceService } from '../Services/student-attendance.service'
import { UserDataService } from '../Services/user-data.service'
import { FormGroup,  FormBuilder, Validators } from "@angular/forms"
import { IstudentAttendanceData } from '../home/model/IstudentAttendanceData';
@Component({
  selector: 'app-user-data-summary',
  templateUrl: './user-data-summary.component.html',
  styleUrls: ['./user-data-summary.component.css']
})
export class UserDataSummaryComponent implements OnInit {
  studentData: Array<IstudentAttendanceData>= [];//storing studentAttendanceData for displaying data to user
  displayData: Array<IstudentAttendanceData>= [];//storing studentdata if data belongs to currentuser and displaying user entries to user
  count = 0;
  TotalAngularHours: number = 0;
  TotalHtmlHours: number = 0;
  TotalCssHours: number = 0;
  TotalJavaScriptHours: number = 0;
  TotalTypeScriptHours: number = 0;
  TotalHours: number = 0;
  Table_Rows:any=["Date","Angular","Html","Css","JavaScript","TypeScript"];
  range: FormGroup
  constructor( private formbuilder: FormBuilder, private student: StudentAttendanceService, private getData: UserDataService) {
    this.range = formbuilder.group({
      start: [],
      end: []
    });
  }
  ngOnInit() {
    //getting studentAttendancedata from server and loading it into StudentData[] Variable
    this.student.Get()
      .subscribe((response) => {
        console.log(response)
        this.studentData = response
        console.log(this.studentData)
      });
  }
  handleChange(event: any) {//if change in date range occurs we will display data occording to the user
    this.displayData.length = 0;
this.TotalAngularHours= 0;
 this.TotalHtmlHours= 0;
 this.TotalCssHours  = 0;
 this.TotalJavaScriptHours = 0;
 this.TotalTypeScriptHours = 0;
 this.TotalHours = 0;
    
  
    this.studentData.forEach((i:any) => {
      if ((i.userId) == this.getData.UserId && (i.Date).slice(7, 10) >= (this.range.get("start")?.value + 1).slice(7, 10) && (i.Date).slice(7, 10) <= (this.range.get("end")?.value + 1).slice(7, 10)) {
        this.displayData.push(i);
        console.log(this.displayData)
      }
    }
    )
    this.displayData.forEach((element: any) => {
      this.TotalAngularHours += Number(element.Angular)
      this.TotalHtmlHours += Number(element.Html)
      this.TotalCssHours += Number(element.Css)
      this.TotalJavaScriptHours += Number(element.JavaScript)
      this.TotalTypeScriptHours += Number(element.TypeScript)
      this.TotalHours = this.TotalAngularHours + this.TotalCssHours + this.TotalHtmlHours + this.TotalJavaScriptHours + this.TotalTypeScriptHours;
    })
  }
  
}









