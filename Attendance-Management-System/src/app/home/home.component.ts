import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../Services/user-data.service'
import { LoginClass } from '../registration/model/login-class'
import { StudentAttendanceService } from '../Services/student-attendance.service'
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms"
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http';

import{IstudentAttendanceData} from '../home/model/IstudentAttendanceData'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  data: Array<LoginClass> = [];//storing Registration details data for displaying current user name
  postData: any = [];// array for posting collected data to server
  Hours: any = [0, 1, 2, 3]// to loop hours in select option
  subjects:any=["Angular","Html","Css","JavaScript","TypeScript"];
  WelcomeName = '';
  StudentAttendanceData:Array<IstudentAttendanceData>=[]; //storing  studentAttendance data to check whether record already exists or not
  idArr:any=[0,0,0,0,0];
  elementid=0;//to get id of record of matching date entered for updating the record
  post:boolean = true;//boolean for updating or posting data to server
  firstDayofWeek = new Date();//getting first date
  first = this.firstDayofWeek.getDate() - this.firstDayofWeek.getDay()+1; // First day is the day of the month - the day of the week
  lastDayofWeek=new Date() //getting last date of the week
  last = this.first + 4;
  firstday = new Date(this.firstDayofWeek.setDate(this.first)).toUTCString();
  lastday = new Date(this.lastDayofWeek.setDate(this.last)).toDateString();
 totalhours(){

  return Number(this.idArr[0])+Number(this.idArr[1])+Number(this.idArr[2])+Number(this.idArr[3])+Number(this.idArr[4])
    
 }
 
  HomePage: FormGroup;
  constructor(private getData: UserDataService, private formBuilder: FormBuilder, private studentAttendence: StudentAttendanceService, private router: Router,private _http: HttpClient) 
    
   {
    this.HomePage = formBuilder.group({
      Date: [],
      Angular: [],
      Html: [],
      Css: [],
      JavaScript: [],
      TypeScript: [],
    })
  }

  ngOnInit() {
    //getting data for regestration data for welcoming user
    this.getData.getUserData()
      .subscribe((response) => {
        console.log(response)
        this.data = response
        this.Welcome()//Welcoming username when page loads
      })
    //getting studentAttendance data for checking whether current date record exists or not and update or post data to server
    this.studentAttendence.Get()
    .subscribe((response) => {
    this.StudentAttendanceData=response
    })
  }
  Welcome() {
    //function for getting current username for welcoming
    this.data.forEach(value => {
      if (value.id == this.getData.UserId) {
        console.log(value.UserName)
        this.WelcomeName = <string>value.UserName;
      }
    })
  }
  onSubmit() {
    this.postData = {
      Date: ((this.HomePage.get("Date")?.value)+1).split(" ", 4).join(' '),
      userId: this.getData.UserId,
      Angular: this.HomePage.get('Angular')?.value,
      Html: this.HomePage.get('Html')?.value,
      Css: this.HomePage.get('Css')?.value,
      JavaScript: this.HomePage.get('JavaScript')?.value,
      TypeScript: this.HomePage.get('TypeScript')?.value,
    } 
 console.log(this.HomePage.value)
   this.StudentAttendanceData.forEach((element: any) => {
    if (element.userId == this.getData.UserId && element.Date==(this.HomePage.get("Date")?.value + 1).split(" ", 4).join(' ')) {
         this.elementid=element.id;
        
         this.post=false;//setting post boolean if we are updating 
         console.log(this.post)
      this._http.put<IstudentAttendanceData>( 'http://localhost:3000/StudentAttendance/'+this.elementid, this.postData).subscribe(response => console.log("sucess", response),
       error => console.error("Error!", error));
      }
    });
    if(this.post==true){
      //if we are not updating a record then we post data to server
      console.log(this.HomePage.value)
      this.studentAttendence.Post(this.postData).subscribe(response => console.log("sucess", response),
      error => console.error("Error!", error));
    }
   //navigating to userdatasummary component after posting data to server
    this.router.navigate(['/UserDataSummary'])

  }
}





  

