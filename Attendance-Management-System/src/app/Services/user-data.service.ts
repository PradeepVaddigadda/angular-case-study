import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginClass } from '../registration/model/login-class'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  UserId = <number>0;
  _url = "http://localhost:3000/Registration/"
  constructor(private _http: HttpClient) { }
  register(userData:LoginClass) {
    return this._http.post<LoginClass[]>(this._url,userData)

  }
  getUserData(): Observable<LoginClass[]> {
    return this._http.get<LoginClass[]>(this._url);
  }
  getid(id: number) {
    this.UserId = id;
    console.log(this.UserId)
  }


}

