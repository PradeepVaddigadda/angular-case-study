import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import{IstudentAttendanceData} from '../home/model/IstudentAttendanceData'
@Injectable({
  providedIn: 'root'
})
export class StudentAttendanceService {

  _url = "http://localhost:3000/StudentAttendance"
  constructor(private _http: HttpClient) { }
  Post(userData:IstudentAttendanceData[]) {
    return this._http.post<IstudentAttendanceData[]>(this._url, userData);

  }
  Get():Observable<IstudentAttendanceData[]> {
    return this._http.get<IstudentAttendanceData[]>(this._url)

  }
  put(data:IstudentAttendanceData[]){
    return this._http.put<IstudentAttendanceData[]>(this._url,data);
  }

}


