import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms"
import { Router } from '@angular/router'
import { UserDataService } from '../Services/user-data.service';
import { LoginClass } from '../login/model/login-class'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data:Array<LoginClass>= [];//To store response  of  type  LoginClass Interface into data array from server for user Authorization
  usermessage = <String>"";//To print Message to user in span tag  if data authorization fails;
  LoginDetailsError:boolean=false;
  LoginDetailsErrorMessage ="";//
  Login: FormGroup;
  constructor(private formBuilder: FormBuilder, private getData: UserDataService, private router: Router) {
    this.Login = formBuilder.group({
      UserName: ["", Validators.required],
      Password: ["", Validators.required]
    })
  }
  ngOnInit() {
    //Getting User Registration data from server for Authorization
    this.getData.getUserData()
      .subscribe((response) => {
        console.log(response)
        this.data = response

      })
  }
  ValidateCredentials() {
    this.data.forEach(value => {
      if (value.UserName == this.Login.get('UserName')?.value && value.Password == this.Login.get("Password")?.value) {
        this.getData.getid(value.id);
        this.router.navigate(['/Home'])
      }
      else if (value.UserName != this.Login.get('UserName')?.value && value.Password != this.Login.get("Password")?.value) {
        this.LoginDetailsError=true;
        this.LoginDetailsErrorMessage = "Invalid Credentials Please Enter Valid Details";
      }
    });
  }
}