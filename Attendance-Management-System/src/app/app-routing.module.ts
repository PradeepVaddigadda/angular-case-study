import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginRegistraionComponent } from './login-registraion/login-registraion.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UserDataSummaryComponent } from './user-data-summary/user-data-summary.component';

const routes: Routes = [ {path:'',redirectTo:'/Login-Registration',pathMatch:'full'},
{path:'Login',component:LoginComponent },
{path:'Registration',component:RegistrationComponent },
{path:'Home',component:HomeComponent},
{path:'UserDataSummary',component:UserDataSummaryComponent},
  {path:'Login-Registration',component:LoginRegistraionComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
