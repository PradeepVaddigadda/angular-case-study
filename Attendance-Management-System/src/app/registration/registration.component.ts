import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder, Validators } from "@angular/forms"
import { UserDataService } from '../Services/user-data.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  Registration: FormGroup
  constructor(private formbuilder: FormBuilder, private _postdata: UserDataService, private router: Router) {
    this.Registration = formbuilder.group({
      UserName: ["", Validators.required],
      Age: ["", Validators.required],
      MobileNumber: ["", Validators.required],
      PinCode: ["", Validators.required],
      Password: ["", Validators.required],
    });
  }
  ngOnInit(): void {
  }
  postData() {
    this._postdata.register(this.Registration.value).subscribe(response => console.log("sucess", response),
      error => console.error("Error!", error));
    alert("Registartion Sucessfull Please Login")
    this.router.navigate(['/Login'])
  }
}
